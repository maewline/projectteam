/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arthi
 */
public class Product {

    private int id;
    private String name;
    private String size;
    private int level;
    private double price;
    private String image;

    public Product(int id, String name, String size, int level, double price, String image) {
        this.id = id;
        this.name = name;
        this.size = size;
        this.level = level;
        this.price = price;
        this.image = image;
    }

    public Product(String name, String size, int level, double price, String image) {
        this.id = -1;
        this.name = name;
        this.size = size;
        this.level = level;
        this.price = price;
        this.image = image;
    }

    public Product() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", size=" + size + ", level=" + level + ", price=" + price + ", image=" + image + '}';
    }

    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("PD_ID"));
            product.setName(rs.getString("PD_Name"));
            product.setSize(rs.getString("PD_Size"));
            product.setLevel(rs.getInt("PD_Level"));
            product.setPrice(rs.getDouble("PD_Price"));
            product.setImage(rs.getString("PD_Image"));
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }

}

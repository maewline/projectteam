/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectteam.poc;

import com.mycompany.projectteam.dao.ProductDao;
import com.mycompany.projectteam.model.Product;

/**
 *
 * @author Arthi
 */
public class TestProductDao {
    public static void main(String[] args) {
        Product product = new Product(158,"น้ำเขียว","2", 1, 60, "greenDrink.jpg");
        ProductDao productDao = new ProductDao();
        productDao.save(product);
        
    }
}
